<?php

return [
    'addresses' => [
        'usuario' => env('USUARIO_SDK_BASE_URL'),
        'pagamento' => env('CORRIDA_SDK_BASE_URL'),
        'corrida' => env('PAGAMENTO_SDK_BASE_URL'),
    ],
    'cache_sdk_calls' => env('CACHE_SDK_CALLS', false),
    'super_user_token' => env('SUPER_USER_TOKEN')
];
