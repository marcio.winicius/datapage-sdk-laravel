## UsuarioResource
```php
$usuarioSDK->findAllUsuarios($query = '', $headers = [])
$usuarioSDK->findByUsuarioId($id, $query = '', $headers = [])
$usuarioSDK->createUsuario(array $data, $headers = [])
$usuarioSDK->updateUsuario(array $data, $id, $headers = [])
$usuarioSDK->deleteUsuario($id, $headers = [])
```

## PermissionsResource
```php
$usuarioSDK->findAllPermissions($query = '', $headers = [])
$usuarioSDK->findByPermissionId($id, $query = '', $headers = [])
$usuarioSDK->createPermission(array $data, $headers = [])
$usuarioSDK->updatePermission(array $data, $id, $headers = [])
$usuarioSDK->deletePermission($id, $headers = [])
```

## RolesResource
```php
$usuarioSDK->findAllRoles($query = '', $headers = [])
$usuarioSDK->findByRoleId($id, $query = '', $headers = [])
$usuarioSDK->createRole(array $data, $headers = [])
$usuarioSDK->updateRole(array $data, $id, $headers = [])
$usuarioSDK->deleteRole($id, $headers = [])
```
