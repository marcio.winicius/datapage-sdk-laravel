<?php
namespace Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits;

use Datapage\DatapageSDK\Support\Enums\HttpMethod;
use Datapage\DatapageSDK\Support\RequestBuilder;

trait RolesResource
{
    public function findAllRoles($query = '', $headers = [])
    {
        $request = new RequestBuilder("{$this->baseUrl}/api/v1/roles?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function findByRoleId($id, $query = '', $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/roles/{$id}?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function createRole(array $data, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/roles", __FUNCTION__);
        $request->withMethod(HttpMethod::POST)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }

    public function updateRole(array $data, $id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/roles/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::PUT)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }


    public function deleteRole($id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/roles/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::DELETE)
            ->withHeaders($headers);

        return $this->parse($request);
    }
}
