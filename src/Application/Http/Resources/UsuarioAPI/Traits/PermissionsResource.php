<?php
namespace Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits;

use Datapage\DatapageSDK\Support\Enums\HttpMethod;
use Datapage\DatapageSDK\Support\RequestBuilder;

trait PermissionsResource
{
    public function findAllPermissions($query = '', $headers = [])
    {
        $request = new RequestBuilder("{$this->baseUrl}/api/v1/permissions?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function findByPermissionId($id, $query = '', $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/permissions/{$id}?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function createPermission(array $data, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/permissions", __FUNCTION__);
        $request->withMethod(HttpMethod::POST)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }

    public function updatePermission(array $data, $id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/permissions/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::PUT)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }


    public function deletePermission($id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/permissions/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::DELETE)
            ->withHeaders($headers);

        return $this->parse($request);
    }
}
