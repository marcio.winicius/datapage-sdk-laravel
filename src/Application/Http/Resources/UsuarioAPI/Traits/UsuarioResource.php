<?php
namespace Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits;

use Datapage\DatapageSDK\Support\Enums\HttpMethod;
use Datapage\DatapageSDK\Support\RequestBuilder;

trait UsuarioResource
{
    public function findAllUsuarios($query = '', $headers = [])
    {
        $request = new RequestBuilder("{$this->baseUrl}/api/v1/usuarios?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function findByUsuarioId($id, $query = '', $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/usuarios/{$id}?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function createUsuario(array $data, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/usuarios", __FUNCTION__);
        $request->withMethod(HttpMethod::POST)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }

    public function updateUsuario(array $data, $id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/usuarios/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::PUT)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }


    public function deleteUsuario($id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/usuarios/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::DELETE)
            ->withHeaders($headers);

        return $this->parse($request);
    }
}
