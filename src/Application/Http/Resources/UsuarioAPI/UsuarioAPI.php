<?php
namespace Datapage\DatapageSDK\Http\Resources\FinanceAPI;

use Datapage\DatapageSDK\Http\Resources\BaseResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\PermissionsResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\RolesResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\UsuarioResource;
use Illuminate\Support\Facades\Config;

class UsuarioAPI extends BaseResource
{
    use UsuarioResource;
    use RolesResource;
    use PermissionsResource;

    function __construct()
    {
        $this->baseUrl = Config::get('datapage_sdk.addresses.usuario');
    }
}
