<?php

namespace Datapage\DatapageSDK\Http\Resources;

use Datapage\DatapageSDK\Support\RequestBuilder;
use Datapage\DatapageSDK\Support\ResponseObject;
use Datapage\DatapageSDK\Support\Traits\Cacheable;
use Illuminate\Support\Facades\Config;

class BaseResource
{
    use Cacheable;

    protected $baseUrl;

    protected function parse(RequestBuilder $builder)
    {
        $key = $this->buildCacheKey(get_called_class(), $builder->callerFunction, $builder->url);

        if($this->isCacheAllowed($builder->callerFunction)) {
            if($cache = $this->hasCache($key)) {
                return new ResponseObject($cache);
            }
        }

        $response = new ResponseObject($builder->send());

        if($this->isCacheAllowed($builder->callerFunction)) {
            $this->putOnCache($key, $response->toArray());
        }

        return $response;
    }
}
