<?php
namespace Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits;

use Datapage\DatapageSDK\Support\Enums\HttpMethod;
use Datapage\DatapageSDK\Support\RequestBuilder;

trait CorridaResource
{
    public function findAllCorridas($query = '', $headers = [])
    {
        $request = new RequestBuilder("{$this->baseUrl}/api/v1/corridas?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function findByCorridaId($id, $query = '', $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/corridas/{$id}?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function createCorrida(array $data, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/corridas", __FUNCTION__);
        $request->withMethod(HttpMethod::POST)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }

    public function updateCorrida(array $data, $id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/corridas/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::PUT)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }


    public function deleteCorrida($id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/corridas/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::DELETE)
            ->withHeaders($headers);

        return $this->parse($request);
    }
}
