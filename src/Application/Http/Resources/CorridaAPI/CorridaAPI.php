<?php
namespace Datapage\DatapageSDK\Http\Resources\FinanceAPI;

use Datapage\DatapageSDK\Http\Resources\BaseResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\CorridaResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\PermissionsResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\RolesResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\UsuarioResource;
use Illuminate\Support\Facades\Config;

class CorridaAPI extends BaseResource
{
    use CorridaResource;

    function __construct()
    {
        $this->baseUrl = Config::get('datapage_sdk.addresses.corrida');
    }
}
