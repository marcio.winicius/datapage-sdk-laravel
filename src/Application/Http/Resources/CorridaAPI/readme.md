## CorridaResource
```php
$corridaSDK->findAllCorridas($query = '', $headers = [])
$corridaSDK->findByCorridaId($id, $query = '', $headers = [])
$corridaSDK->createCorrida(array $data, $headers = [])
$corridaSDK->updateCorrida(array $data, $id, $headers = [])
$corridaSDK->deleteCorrida($id, $headers = [])
```
