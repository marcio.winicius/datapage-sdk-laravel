<?php
namespace Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits;

use Datapage\DatapageSDK\Support\Enums\HttpMethod;
use Datapage\DatapageSDK\Support\RequestBuilder;

trait PagamentoResource
{
    public function findAllPagamentos($query = '', $headers = [])
    {
        $request = new RequestBuilder("{$this->baseUrl}/api/v1/pagamentos?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function findByPagamentoId($id, $query = '', $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/pagamentos/{$id}?{$query}", __FUNCTION__);
        $request->withHeaders($headers);

        return $this->parse($request);
    }

    public function createPagamento(array $data, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/pagamentos", __FUNCTION__);
        $request->withMethod(HttpMethod::POST)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }

    public function updatePagamento(array $data, $id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/pagamentos/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::PUT)
            ->withBody($data)
            ->withHeaders($headers);

        return $this->parse($request);
    }


    public function deletePagamento($id, $headers = [])
    {
        $request = new RequestBuilder( "{$this->baseUrl}/api/v1/pagamentos/{$id}", __FUNCTION__);
        $request->withMethod(HttpMethod::DELETE)
            ->withHeaders($headers);

        return $this->parse($request);
    }
}
