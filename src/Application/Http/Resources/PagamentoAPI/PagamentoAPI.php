<?php
namespace Datapage\DatapageSDK\Http\Resources\FinanceAPI;

use Datapage\DatapageSDK\Http\Resources\BaseResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\PagamentoResource;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\Traits\FormaPagamentoResource;
use Illuminate\Support\Facades\Config;

class PagamentoAPI extends BaseResource
{
    use PagamentoResource;
    use FormaPagamentoResource;

    function __construct()
    {
        $this->baseUrl = Config::get('datapage_sdk.addresses.pagamento');
    }
}
