## PagamentoResource
```php
$pagamentoSDK->findAllPagamentos($query = '', $headers = [])
$pagamentoSDK->findByPagamentoId($id, $query = '', $headers = [])
$pagamentoSDK->createPagamento(array $data, $headers = [])
$pagamentoSDK->updatePagamento(array $data, $id, $headers = [])
$pagamentoSDK->deletePagamento($id, $headers = [])
```

## FormaPagamentoResource
```php
$pagamentoSDK->findAllFormasPagamentos($query = '', $headers = [])
$pagamentoSDK->findByFormaPagamentoId($id, $query = '', $headers = [])
$pagamentoSDK->createFormaPagamento(array $data, $headers = [])
$pagamentoSDK->updateFormaPagamento(array $data, $id, $headers = [])
$pagamentoSDK->deleteFormaPagamento($id, $headers = [])
```
