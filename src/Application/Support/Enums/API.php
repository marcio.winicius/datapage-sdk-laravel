<?php

namespace Datapage\DatapageSDK\Support\Enums;

class API
{
    const USUARIO = "usuario";
    const PAGAMENTO = "pagamento";
    const CORRIDA = "corrida";
}
