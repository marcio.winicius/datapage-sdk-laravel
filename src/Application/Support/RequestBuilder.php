<?php

namespace Datapage\DatapageSDK\Support;

use Datapage\DatapageSDK\Exceptions\Base\DatapageSDKException;
use Datapage\DatapageSDK\Facades\HttpClient;
use Datapage\DatapageSDK\Support\Enums\HttpMethod;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class RequestBuilder
{
    public $method;
    public $url;
    public $body;
    public $headers;
    public $callerFunction;
    public $multipart;

    public function __construct($url, $callerFunction)
    {
        $this->method = HttpMethod::GET;
        $this->url = $url;
        $this->callerFunction = $callerFunction;
    }

    public function withBody(array $body)
    {
        $this->body = $body;
        return $this;
    }

    public function withHeaders(array $headers)
    {
        $this->headers = $headers;
        return $this;
    }

    public function withMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    public function withMultipart(array $data)
    {
        $this->multipart = $data;
    }

    public function send()
    {
        try {
            if($this->method == HttpMethod::POST and isset($this->multipart)) {
                return HttpClient::post($this->url, ['multipart' => $this->multipart, 'headers' => $this->headers])->getBody()->getContents();
            }

            $params = ['form_params' => $this->body];
            if($this->headers) {
                $params = ['headers' => $this->headers, 'form_params' => $this->body];
            }

            return HttpClient::{$this->method}($this->url, $params)->getBody()->getContents();
        } catch (ClientException | ServerException $exception ) {
            throw new DatapageSDKException($this->parseMessages($exception), $exception->getCode());
        }
    }

    private function parseMessages($exception)
    {
        return json_encode(['message' => json_decode($exception->getResponse()->getBody()->getContents())->message]);
    }
}
