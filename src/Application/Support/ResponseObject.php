<?php

namespace Datapage\DatapageSDK\Support;

class ResponseObject
{
    public $data;
    public $meta;

    private $response;

    public function __construct($response)
    {
        $this->response = $response;

        $this->data = json_decode($response)->data ?? null;
        $this->meta = json_decode($response)->meta ?? null;
    }

    public function toArray()
    {
        return json_decode($this->response, true);
    }
}
