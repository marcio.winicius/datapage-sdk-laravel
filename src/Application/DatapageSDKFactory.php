<?php

namespace Datapage\DatapageSDK;

use Datapage\DatapageSDK\Exceptions\ServiceNotImplementedException;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\CorridaAPI;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\PagamentoAPI;
use Datapage\DatapageSDK\Http\Resources\FinanceAPI\UsuarioAPI;
use Datapage\DatapageSDK\Support\Enums\API;

class DatapageSDKFactory
{
    public static function get($api)
    {
        switch ($api) {
            case API::USUARIO:
                return new UsuarioAPI();
            case API::PAGAMENTO:
                return new PagamentoAPI();
            case API::CORRIDA:
                return new CorridaAPI();
            default:
                throw new ServiceNotImplementedException("Micro serviço ainda não foi implementado", 400);
        }
    }
}
