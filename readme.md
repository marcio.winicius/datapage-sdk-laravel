# Datapage SDK

## Usage Package

> Add dependency to your composer.json
```
"repositories": [
    ...
    {
        "type": "git",
        "url": "[git@git2.datapage.com.br:24]:ms/datapage-sdk-laravel.git"
    }
],
"require": {
    ...
    "ms/datapage-sdk-laravel": "dev-develop"
}
```

> Run composer update only for this package

```
$ composer update ms/datapage-sdk-laravel
```

> Add provider in config/app.php

```
'providers' => [
    ...
    Datapage\DatapageSDK\Providers\DatapageSDKProvider::class,
];
```

> Configure an alias to facade

```
'aliases' => [
    ...
    'DatapageSDK' => Datapage\DatapageSDK\Facades\DatapageSDK::class,
]
```

> If this is the first SDK used in the project 

```
$ php artisan vendor:publish --tag="datapage-sdk-config" --force
```

> configure base urls in docker-compose.yml or .env file

```
USUARIO_SDK_BASE_URL=
CORRIDA_SDK_BASE_URL=
PAGAMENTO_SDK_BASE_URL=

SUPER_USER_TOKEN="Bearer akspoksaopaskposkaopask"
```

> If use cache

```
CACHE_SDK_CALLS={true|false}
```

## Read the docs:

- [UsuarioAPI](src/Application/Http/Resources/UsuarioAPI/readme.md)
- [CorridaAPI](src/Application/Http/Resources/CorridaAPI/readme.md)
- [PagamentoAPI](src/Application/Http/Resources/PagamentoAPI/readme.md)
